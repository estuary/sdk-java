/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.sdk.execption.ExceptionType;

public class SdkException extends Exception {

    private ExceptionType exceptionType;

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public int getCode() {
        return exceptionType.getCode();
    }


    public SdkException(String message) {
        super(message);
        this.exceptionType = ExceptionType.OTHER;
    }

    public SdkException(String message, ExceptionType exceptionType) {
        super(message);
        this.exceptionType = exceptionType;
    }
}

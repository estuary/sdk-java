/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.execption;

/**
 * 异常类型
 */
public enum ExceptionType {

    // not normal connect
    NOTNORMALCONNECT(1, "没有可用的连接"),

    // invoke time out
    TIMEOUT(2, "超时异常"),

    // invoke exec error
    EXECUTION(3, "执行异常"),

    // invoke interrupted
    INTERRUPTED(4, "中断异常"),

    // archive center grpc connect fail
    CONNECT(5, "连接异常"),

    // archive block error
    ARCHIVEBLOCK(6, "归档区块异常"),

    // when invoke tx, client error, need to invalidate client.
    // invalidate error.
    INVALIDATECLIENT(7, "连接无效化异常"),

    OTHER(500, "其他异常"),
    ;
    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    ExceptionType(int code, String name) {
        this.code = code;
        this.msg = name;
    }
}

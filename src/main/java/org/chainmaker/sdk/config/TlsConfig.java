/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.config;


/**
 * 归档中心tls信息
 */
public class TlsConfig {

    // tls hostname
    private String serverName;

    // 私钥
    private String privKeyFile;

    // 证书
    private String certFile;

    // ca证书路径列表
    private String[] trustCaList;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getPrivKeyFile() {
        return privKeyFile;
    }

    public void setPrivKeyFile(String privKeyFile) {
        this.privKeyFile = privKeyFile;
    }

    public String getCertFile() {
        return certFile;
    }

    public void setCertFile(String certFile) {
        this.certFile = certFile;
    }

    public String[] getTrustCaList() {
        return trustCaList;
    }

    public void setTrustCaList(String[] trustCaList) {
        this.trustCaList = trustCaList;
    }

    public void setServer_name(String server_name) {
        this.serverName = server_name;
    }

    public void setPriv_key_file(String priv_key_file) {
        this.privKeyFile = priv_key_file;
    }

    public void setCert_file(String cert_file) {
        this.certFile = cert_file;
    }

    public void setTrust_ca_list(String[] trust_ca_list) {
        this.trustCaList = trust_ca_list;
    }
}

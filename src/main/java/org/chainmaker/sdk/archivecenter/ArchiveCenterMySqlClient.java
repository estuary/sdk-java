/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zayk.util.encoders.Hex;
import org.apache.commons.dbutils.*;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.chainmaker.pb.archivecenter.Archivecenter;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.pb.store.Store;
import org.chainmaker.sdk.ChainClient;
import org.chainmaker.sdk.ChainClientException;
import org.chainmaker.sdk.config.ArchiveConfig;
import org.chainmaker.sdk.execption.ExceptionType;
import org.chainmaker.sdk.model.BlockInfo;
import org.chainmaker.sdk.model.Sysinfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 归档中心musql实现
 */
public class ArchiveCenterMySqlClient implements ArchiveService {

    private HikariDataSource ds = null;

    private QueryRunner queryRunner;

    private RowProcessor rowProcessor;

    private ChainClient chainClient;

    private final String KArchivedblockheight = "archived_block_height";
    private ArchiveConfig archiveConfig;

    private String chainId;

    private static final Logger logger = LoggerFactory.getLogger(ArchiveCenterHttpClient.class);

    public HikariDataSource getDs() {
        return ds;
    }

    public void setDs(HikariDataSource ds) {
        this.ds = ds;
    }

    public ChainClient getChainClient() {
        return chainClient;
    }

    public void setChainClient(ChainClient chainClient) {
        this.chainClient = chainClient;
    }

    public ArchiveConfig getArchiveConfig() {
        return archiveConfig;
    }

    public void setArchiveConfig(ArchiveConfig archiveConfig) {
        this.archiveConfig = archiveConfig;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public ArchiveCenterMySqlClient(String chainId, ArchiveConfig archiveConfig, ChainClient chainClient) throws ChainClientException {
        this.archiveConfig = archiveConfig;
        this.chainId = chainId;
        HikariConfig config = new HikariConfig();
        String[] strings = archiveConfig.getDest().split(":");
        config.setJdbcUrl("jdbc:mysql://" + strings[2] + ":" + strings[3] + "/cm_archived_chain_" + chainId);
        config.setUsername(strings[0]);
        config.setPassword(strings[1]);
        config.setMaximumPoolSize(10);
        try {
            this.ds = new HikariDataSource(config);
        } catch (Exception e) {
            if (e.getMessage().contains("Unknown database")) {
                this.initDatabase();
                this.ds = new HikariDataSource(config);
            }
        }
        queryRunner = new QueryRunner(this.ds);
        BeanProcessor processor = new GenerousBeanProcessor();
        this.rowProcessor = new BasicRowProcessor(processor);
        this.chainClient = chainClient;
    }

    public void initDatabase() throws ChainClientException {
        HikariConfig config = new HikariConfig();
        String[] strings = this.archiveConfig.getDest().split(":");
        String url = "jdbc:mysql://" + strings[2] + ":" + strings[3];
        config.setJdbcUrl(url);
        config.setUsername(strings[0]);
        config.setPassword(strings[1]);
        config.setMaximumPoolSize(10);
        try (HikariDataSource hikariDataSource = new HikariDataSource(config)) {
            String database = "cm_archived_chain_" + this.chainId;
            Connection connection = hikariDataSource.getConnection();
            int res = connection.createStatement().executeUpdate("create database if not exists " + database);
            if (res <= 0) {
                throw new ChainClientException("create database fail");
            }
        } catch (Exception e) {
            throw new ChainClientException("create database fail, error:" + e.getMessage());
        }
    }

    @Override
    public ChainmakerTransaction.TransactionInfo getTxByTxId(String txId, long rpcCallTimeout)  throws ChainClientException{
        long height = 0L;
        try {
            height = chainClient.getBlockHeightByTxId(txId, rpcCallTimeout);
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        ChainmakerBlock.BlockInfo block = this.getBlockByHeight(height, false, rpcCallTimeout);
        ChainmakerTransaction.TransactionInfoWithRWSet transactionInfoWithRWSet = this.getTxByTxIdInBlock(block, txId, false);
        return ChainmakerTransaction.TransactionInfo.newBuilder()
                .setTransaction(transactionInfoWithRWSet.getTransaction())
                .setBlockHeight(transactionInfoWithRWSet.getBlockHeight())
                .setBlockHash(transactionInfoWithRWSet.getBlockHash())
                .setTxIndex(transactionInfoWithRWSet.getTxIndex())
                .setBlockTimestamp(transactionInfoWithRWSet.getBlockTimestamp())
                .build();
    }

    @Override
    public ChainmakerTransaction.TransactionInfoWithRWSet getTxWithRWSetByTxId(String txId, long rpcCallTimeout)  throws ChainClientException {
        long height = 0L;
        try {
            height = chainClient.getBlockHeightByTxId(txId, rpcCallTimeout);
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        ChainmakerBlock.BlockInfo block = this.getBlockByHeight(height, true, rpcCallTimeout);
        return this.getTxByTxIdInBlock(block, txId, true);
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByHeight(long blockHeight, boolean withRWSet, long rpcCallTimeout) throws ChainClientException {
        ChainmakerBlock.BlockInfo blockInfo;
        try {
            String tableName = BlockInfo.BlockInfoTableNameByBlockHeight(blockHeight);
            BlockInfo dbBlockInfo = queryRunner.query("select * from " + tableName + " where Fblock_height=? AND Fis_archived=1", new BeanHandler<>(BlockInfo.class, rowProcessor), blockHeight);
            Store.BlockWithRWSet blockWithRWSet = Store.BlockWithRWSet.parseFrom(dbBlockInfo.getFblockWithRwset());
            ChainmakerBlock.BlockInfo.Builder builder = ChainmakerBlock.BlockInfo.newBuilder().setBlock(blockWithRWSet.getBlock());
            if (withRWSet) {
                builder.addAllRwsetList(blockWithRWSet.getTxRWSetsList());
            }
            blockInfo = builder.build();
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        return blockInfo;
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByHash(String blockHash, boolean withRWSet, long rpcCallTimeout)  throws ChainClientException {
        long height = 0L;
        try {
            height = chainClient.getBlockHeightByBlockHash(blockHash, rpcCallTimeout);
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        return this.getBlockByHeight(height, withRWSet, rpcCallTimeout);
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByTxId(String txId, boolean withRWSet, long rpcCallTimeout)  throws ChainClientException {
        long height = 0L;
        try {
            height = chainClient.getBlockHeightByTxId(txId, rpcCallTimeout);
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        return this.getBlockByHeight(height, withRWSet, rpcCallTimeout);
    }

    @Override
    public ChainConfigOuterClass.ChainConfig getChainConfigByBlockHeight(long blockHeight, long rpcCallTimeout) throws ChainClientException {
        try {
            Archivecenter.ArchiveStatusResp resp= this.getArchivedStatus(rpcCallTimeout);
            if (blockHeight <= resp.getArchivedHeight()) {
                ChainmakerBlock.BlockInfo blockInfo = this.getBlockByHeight(blockHeight, false, rpcCallTimeout);
                if (blockInfo.getBlock().getHeader().getBlockType() == ChainmakerBlock.BlockType.CONFIG_BLOCK) {
                    return getChainConfig(blockInfo.getBlock().getTxs(0));
                }
                long preBlockHeight = blockInfo.getBlock().getHeader().getPreConfHeight();
                blockInfo = this.getBlockByHeight(preBlockHeight, false, rpcCallTimeout);
                if (blockInfo.getBlock().getHeader().getBlockType() == ChainmakerBlock.BlockType.CONFIG_BLOCK) {
                    return getChainConfig(blockInfo.getBlock().getTxs(0));
                }
            }
        }catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        return null;
    }

    private ChainConfigOuterClass.ChainConfig getChainConfig(ChainmakerTransaction.Transaction transaction)  throws ChainClientException{
        try {
            return ChainConfigOuterClass.ChainConfig.parseFrom(transaction.getResult().getContractResult().getResult().toByteArray());
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
    }

    @Override
    public void register(ChainmakerBlock.BlockInfo genesis, long rpcCallTimeout) throws ChainClientException {
        try {
            this.queryRunner.update("CREATE TABLE IF NOT exists`sysinfo` (\n" +
                    "  `Fid` int unsigned NOT NULL AUTO_INCREMENT,\n" +
                    "  `Fcreate_time` timestamp  NOT NULL DEFAULT current_timestamp,\n" +
                    "  `Fmodify_time` timestamp  DEFAULT current_timestamp on update current_timestamp,\n" +
                    "  `Fdelete_time` timestamp  NULL,\n" +
                    "  `k` varchar(64) NOT NULL,\n" +
                    "  `v` varchar(8000) NOT NULL,\n" +
                    "  PRIMARY KEY ( `Fid` ), \n" +
                    "  KEY `sysinfo_Fcreate_time_index` (`Fcreate_time`),\n" +
                    "  UNIQUE KEY `sysinfo_pk`(`k`) \n" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
            this.queryRunner.update("insert into `sysinfo` values (null, ?, null, null, ?, ?)", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), KArchivedblockheight, 0);
            this.queryRunner.update("CREATE TABLE IF NOT exists `t_block_info_1` (\n" +
                    "  `Fid` int unsigned NOT NULL AUTO_INCREMENT,\n" +
                    "  `Fcreate_time` timestamp  NOT NULL DEFAULT current_timestamp,\n" +
                    "  `Fmodify_time` timestamp  DEFAULT current_timestamp on update current_timestamp,\n" +
                    "  `Fdelete_time` timestamp  NULL,\n" +
                    "  `Fchain_id` varchar(64) NOT NULL,\n" +
                    "  `Fblock_height` int unsigned NOT NULL,\n" +
                    "  `Fblock_with_rwset` longblob NOT NULL,\n" +
                    "  `Fhmac` varchar(64) NOT NULL,\n" +
                    "  `Fis_archived` tinyint(1) NOT NULL DEFAULT '0',\n" +
                    "  PRIMARY KEY ( `Fid` ), \n" +
                    "  KEY `sysinfo_Fcreate_time_index` (`Fcreate_time`),\n" +
                    "  UNIQUE KEY `idx_blockheight`(`Fblock_height`)\n" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.archiveBlock(genesis);
    }

    @Override
    public void archiveBlock(ChainmakerBlock.BlockInfo block) throws ChainClientException {
        QueryRunner qr = new QueryRunner();
        long height = block.getBlock().getHeader().getBlockHeight();
        String tableName = BlockInfo.BlockInfoTableNameByBlockHeight(height);
        if (height % 10000 == 0) {
            try {
                this.queryRunner.update("CREATE TABLE IF NOT exists `" + tableName + "` (\n" +
                        "  `Fid` int unsigned NOT NULL AUTO_INCREMENT,\n" +
                        "  `Fcreate_time` timestamp  NOT NULL DEFAULT current_timestamp,\n" +
                        "  `Fmodify_time` timestamp  DEFAULT current_timestamp on update current_timestamp,\n" +
                        "  `Fdelete_time` timestamp  NULL,\n" +
                        "  `Fchain_id` varchar(64) NOT NULL,\n" +
                        "  `Fblock_height` int unsigned NOT NULL,\n" +
                        "  `Fblock_with_rwset` longblob NOT NULL,\n" +
                        "  `Fhmac` varchar(64) NOT NULL,\n" +
                        "  `Fis_archived` tinyint(1) NOT NULL DEFAULT '0',\n" +
                        "  PRIMARY KEY ( `Fid` ), \n" +
                        "  KEY `sysinfo_Fcreate_time_index` (`Fcreate_time`), \n" +
                        "  UNIQUE KEY `idx_blockheight`(`Fblock_height`)\n" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
            } catch (SQLException e) {
                throw new ChainClientException(e.getMessage(), ExceptionType.ARCHIVEBLOCK);
            }
        }
        Connection connection = null;
        try {
            connection = ds.getConnection();
            connection.setAutoCommit(false);
            BlockInfo blockInfo = qr.query(connection, "select * from " + tableName + " where Fblock_height = ? limit 1", new BeanHandler<>(BlockInfo.class, rowProcessor), height);
            if (blockInfo != null) {
                if (!blockInfo.isFisArchived()) {
                    qr.update(connection, "update " + tableName + " set Fis_archived = 'true' where Fchain_id = ? and Fblock_height = ?", blockInfo.getFchainId(), blockInfo.getFblockHeight());
                }
            } else {
                Store.BlockWithRWSet blockWithRWSet = Store.BlockWithRWSet.newBuilder()
                        .setBlock(block.getBlock())
                        .addAllTxRWSets(block.getRwsetListList())
                        .build();
                String sum = hmac(block.getBlock().getHeader().getChainId(), height, blockWithRWSet.toByteArray(), this.archiveConfig.getSecretKey());
                qr.update(connection,
                        "insert into " + tableName + " values(null, ? , null, null, ?, ?, ?, ?, ?)",
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), block.getBlock().getHeader().getChainId(), height, blockWithRWSet.toByteArray(), sum, true);
                qr.update(connection, "update sysinfo set v = ? where k = ?", height, KArchivedblockheight);
                connection.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    throw new ChainClientException(ex.getMessage(), ExceptionType.ARCHIVEBLOCK);
                }
            }
        }
        // 记住关闭
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new ChainClientException(e.getMessage(), ExceptionType.ARCHIVEBLOCK);
            }
        }
    }

    @Override
    public void archiveBlocks(BlockIterator blockIterator, Notice notice) throws ChainClientException{
        int sendCount = 0;
        while (blockIterator.next()) {
            ChainmakerBlock.BlockInfo blockInfo = null;
            try {
                blockInfo = blockIterator.value();
            } catch (ChainClientException e) {
                notice.heightNotice(new ProcessMessage(blockIterator.current(), e.getMessage()));
                continue;
            }
            try {
                this.archiveBlock(blockInfo);
            } catch (ChainClientException e) {
                notice.heightNotice(new ProcessMessage(blockInfo.getBlock().getHeader().getBlockHeight(), e.getMessage()));
            }
            sendCount++;
            notice.heightNotice(new ProcessMessage(blockInfo.getBlock().getHeader().getBlockHeight()));
        }
        if (sendCount == 0) {
            throw new ChainClientException("no block to archive");
        }
    }

    @Override
    public Archivecenter.ArchiveStatusResp getArchivedStatus(long rpcCallTimeout) throws ChainClientException{
        try {
            Sysinfo sysinfo = queryRunner.query("select * from sysinfo where k = ? limit 1", new BeanHandler<>(Sysinfo.class, rowProcessor), KArchivedblockheight);
            if (sysinfo == null ){
                throw new SQLException("data doesn't exist");
            }
            return Archivecenter.ArchiveStatusResp.newBuilder()
                    .setArchivedHeight(Long.parseLong(sysinfo.getV()))
                    .setInArchive(false)
                    .setCode(0)
                    .build();
        } catch (SQLException exception) {
            if (exception.getMessage().contains("doesn't exist")) {
                return Archivecenter.ArchiveStatusResp.newBuilder()
                        .setInArchive(false)
                        .setArchivedHeight(0)
                        .setMessage("chain genesis not exists")
                        .setCode(0)
                        .build();
            }
            throw new ChainClientException(exception.getMessage());
        }
    }

    public ChainmakerTransaction.TransactionInfoWithRWSet getTxByTxIdInBlock(ChainmakerBlock.BlockInfo blockInfo, String txId, boolean witRWSet) {
        for (int i = 0; i < blockInfo.getBlock().getTxsList().size(); i++) {
            ChainmakerTransaction.Transaction transaction = blockInfo.getBlock().getTxsList().get(i);
            if (transaction.getPayload().getTxId().equals(txId)) {
                ChainmakerTransaction.TransactionInfoWithRWSet.Builder builder = ChainmakerTransaction.TransactionInfoWithRWSet.newBuilder()
                        .setTransaction(transaction)
                        .setBlockHeight(blockInfo.getBlock().getHeader().getBlockHeight())
                        .setBlockHash(blockInfo.getBlock().getHeader().getBlockHash())
                        .setTxIndex(i);
                if (witRWSet) {
                    builder.setRwSet(blockInfo.getRwsetList(i));
                }
                return builder.build();
            }
        }
        return null;
    }

    private String hmac(String chainId, long blkHeight, byte[] blkWithRWSetBytes, String secretKey) {
        SM3Digest digest = new SM3Digest();
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(0, blkHeight);
        byte[] chainIdBytes = chainId.getBytes();
        byte[] secretKeyBytes = secretKey.getBytes();
        byte[] blkHeightBytes = buffer.array();
        int length = chainIdBytes.length+blkHeightBytes.length + blkWithRWSetBytes.length + secretKeyBytes.length;
        byte[] dataBytes = new byte[length];
        System.arraycopy(chainIdBytes, 0, dataBytes, 0, chainIdBytes.length);
        System.arraycopy(blkHeightBytes, 0, dataBytes, chainIdBytes.length, blkHeightBytes.length);
        System.arraycopy(blkWithRWSetBytes, 0, dataBytes, blkHeightBytes.length + chainIdBytes.length, blkWithRWSetBytes.length);
        System.arraycopy(secretKeyBytes, 0, dataBytes, length - secretKeyBytes.length, secretKeyBytes.length);
        digest.update(dataBytes, 0, length);
        byte[] result = new byte[digest.getDigestSize()];
        digest.doFinal(result, 0);
        return Hex.toHexString(result);
    }
}

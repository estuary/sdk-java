/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

public enum ArchiveCenterApi {

    // 根据高度查区块头接口
    GetBlockHeaderByHeight("get_block_header_by_height"),
    // 根据区块hash查块高接口
    GetBlockHeightByHash("get_height_by_hash"),
    // 根据txid查询块高接口
    GetBlockHeightByTxId("get_height_by_tx_id"),
    // 根据块高查全区块
    GetFullBlockByHeight("get_full_block_by_height"),
    // 根据hash查读写集区块接口
    GetBlockWithTxRWSetByHash("get_block_info_by_hash"),
    // 根据txid查读写集区块接口
    GetBlockWithTxRWSetByTxId("get_block_info_by_txid"),
    // 根据高度查读写集区块接口
    GetBlockWithTxRWSetByHeight("get_block_info_by_height"),
    // 根据txid查交易接口
    GetCommonTransactionByTxId("get_transaction_info_by_txid"),
    // 根据txid查读写集交易接口
    GetCommonTransactionWithRWSetByTxId("get_full_transaction_info_by_txid"),
    // 根据块高查询最近链配置信息接口
    GetConfigByHeight("get_chainconfig_by_height"),

    // 根据txid查询merkle path
    GetMerklePathByTxId("get_merklepath_by_txid"),

    ;


    private String value;

    ArchiveCenterApi(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.model;


/**
 * db基础表结构
 */
public class BaseModel {

    // 自增id
    private long fid;

    // 创建时间
    private String fcreateTime;

    // 更新时间
    private String fmodifyTime;

    // 删除时间
    private String fdeleteTime;

    public long getFid() {
        return fid;
    }

    public void setFid(long fid) {
        this.fid = fid;
    }

    public String getFcreateTime() {
        return fcreateTime;
    }

    public void setFcreateTime(String fcreateTime) {
        this.fcreateTime = fcreateTime;
    }

    public String getFmodifyTime() {
        return fmodifyTime;
    }

    public void setFmodifyTime(String fmodifyTime) {
        this.fmodifyTime = fmodifyTime;
    }

    public String getFdeleteTime() {
        return fdeleteTime;
    }

    public void setFdeleteTime(String fdeleteTime) {
        this.fdeleteTime = fdeleteTime;
    }
}

# 2.3.2 版本更新
升级了大量的依赖包，以便可以通过生产环境漏洞扫描。
```shell
org.bouncycastle:bcpkix-jdk15on:1.62 ->org.bouncycastle:bcpkix-jdk18on:1.75
io.netty系列(handle除外) 4.1.53.Final->4.1.79.Final
io.grpc系列 1.23.0->1.53.0

# 特别注意
io.grpc:grpc-netty 需要排除io.netty:netty-handler
implementation 'io.grpc:grpc-netty:1.53.0', {
    exclude group: 'io.netty' ,module: 'netty-handler'
}

org.web3j:abi需要排除bouncycastle
implementation 'org.web3j:abi:5.0.0', {
    exclude group: 'org.bouncycastle'
}
```

1. 新增获取节点归档状态详细信息
   ```   
   getArchiveStatus
   ```
2. 新增代付者处理
   1. 发送合约管理请求（创建、更新、冻结、解冻、吊销）
   ```
   sendContractManageRequestWithPayer
   ```
   2. 发起多签请求(指定gas代扣账户)
   ``` 
   multiSignContractReqWithPayer
   ```
   3. 触发执行多签请求(指定gas代扣账户)
   ```
   multiSignContractTrigWithPayer
   ```
   4. 发起多签投票
   ```
   multiSignContractVoteWithGasLimitAndPayer
   ```   
3. 新增gas limit限制  
   1. 根据发起多签请求所需的参数构建payload
   ```
   createMultiSignReqPayloadWithGasLimit
   ```
   2. 发起多签投票
   ```
   multiSignContractVoteWithGasLimit
   ```
4. 新增根据txId查询多签状态
   ```
   multiSignContractQueryWithParams
   ```
5. 设置链配置的 default gas_price 参数
   ```
   createSetInvokeGasPricePayload
   ```   
6. 设置链配置的 install_base gas 参数
   ```
   createSetInstallBaseGasPayload
   ```    
7. 设置链配置的 install gas_price 参数
   ```
   createSetInstallGasPricePayload
   ```  
8. 新增归档区块（http归档前需要进行使用接口进行注册，其他需要计算高度为0的区块hash放入配置文件）
   ```
   archiveBlocks
   ```  
9. 新增恢复归档区块
   ```
   restoreBlocks
   ```  
10. 兼容归档中心处理（mysql、grpc、http三种模式）
    1. 根据交易id获取交易
    ```
    getTxByTxId
    ```
    2. 根据交易id获取带有读写集的交易
    ```
    getTxWithRWSetByTxId
    ```
    3. 根据区块高度获取区块
    ```
    getBlockByHeight
    ```
    4. 根据区块哈希获取区块
    ```
    getBlockByHash
    ```
    5. 根据交易id获取区块
    ```
    getBlockByTxId
    ```
    5. 根据区块高度获取链配置
    ```
    getChainConfigByBlockHeight
    ```
11. 新增异常类型
    ```
    ExceptionType
    ```
12. 新增jdk17国密支持
    兼容jdk8，jdk11，jdk17国密与非国密

# 2.3.1.3 版本更新    

# 2.3.1.2 版本更新
1. 优化连接池
    ```   
   class GrpcClinetFactory
    ```
2. 优化订阅获取交易结果
    ```   
   class TxResultDispatcher
    ```
3. 优化连接池配置
4. 修改rpc连接超时时间

# 2.3.1 版本更新
1. 优化订阅获取交易结果
    ```   
   class TxResultDispatcher
    ```
2. 优化grpc连接
    ```   
   class GrpcClientFactory
    ```
3. 修复合约名称地址获取bug
   ```
    public static String nameToAddrStr(String data, ChainConfigOuterClass.AddrType addrType) 
   ```
4. 优化根据公钥获取获取ans1模式的byte数组
   ```
   private static byte[] marshalPublicKey(PublicKey publicKey)
   ```
5. 发起多签请求
   ```    
   public ResultOuterClass.TxResponse multiSignContractReq(Request.Payload payload, long rpcCallTimeout,boolean withSyncResult)
   ```
6. 发起多签投票
   ```    
   public ResultOuterClass.TxResponse multiSignContractVote(Request.Payload payload, Request.EndorsementEntry endorsementEntry, boolean isAgree, long rpcCallTimeout, boolean withSyncResult)
   ```
7. 触发执行多签请求
   ```    
   public ResultOuterClass.TxResponse multiSignContractTrig(Request.Payload payload, Request.Limit limit, long rpcCallTimeout, boolean withSyncResult)
   ```
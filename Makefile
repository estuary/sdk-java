.PHONY:build
build:
	./gradlew clean build -x test

.PHONY:copyJars
copyJars:
	./gradlew copyJars